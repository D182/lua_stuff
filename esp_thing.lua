--[[
    ESP Thing by D182
]]--

surface.CreateFont( "hud_font", {
 font = "Trebuchet",
 size = 15,
 weight = 500,
 outline = false
} )

local allowed_ents = {
    "gmod_wire_expression2",
    "crossbow_bolt",
    "rpg_missile",
    "prop_physics",
    "player",
    "grenade_ar2",
    "npc_*",
    "item_*"
}

function is_allowed(ent)
    for k,v in pairs(allowed_ents) do
        if string.match(ent:GetClass(), v) then return true end
    end
    return false
end 

local function coordinates( ent )
    local min, max = ent:OBBMins(), ent:OBBMaxs()
    local corners = {
            Vector( min.x, min.y, min.z ),
            Vector( min.x, min.y, max.z ),
            Vector( min.x, max.y, min.z ),
            Vector( min.x, max.y, max.z ),
            Vector( max.x, min.y, min.z ),
            Vector( max.x, min.y, max.z ),
            Vector( max.x, max.y, min.z ),
            Vector( max.x, max.y, max.z )
    }
     
    local minX, minY, maxX, maxY = ScrW() * 2, ScrH() * 2, 0, 0
    for _, corner in pairs( corners ) do
            local onScreen = ent:LocalToWorld( corner ):ToScreen()
            minX, minY = math.min( minX, onScreen.x ), math.min( minY, onScreen.y )
            maxX, maxY = math.max( maxX, onScreen.x ), math.max( maxY, onScreen.y )
    end
     
    return minX, minY, maxX, maxY
end

hook.Add("HUDPaint", "Crosshair", function()
    local x, y = ScrW() / 2, ScrH() / 2

    for k,v in pairs(ents.GetAll()) do
            if v:GetClass() != "worldspawn" and is_allowed(v) and ( v:IsPlayer() and v:Nick() != LocalPlayer():Nick() or !v:IsPlayer()) then
                local x1,y1,x2,y2 = coordinates(v)
                --print(tostring(team.GetColor(v:Team())))
                
        
                local clr = Color(255,0,0)

                if v:GetClass():match("prop_*") then
                    clr = Color(0,0,255)
                elseif v:IsNPC() then 
                    clr = Color(255,255,0)        
                end    

                local class = v:GetClass()

                if v:IsPlayer()then
                    class = v:Nick()
                    clr = team.GetColor(v:Team())
                end
            
                draw.SimpleText(class, "hud_font", (x1 + x2) / 2, y1 - 15, clr, TEXT_ALIGN_CENTER)

                surface.SetDrawColor( clr )
         
                surface.DrawLine( x1, y1, math.min( x1 + 8, x2 ), y1 )
                surface.DrawLine( x1, y1, x1, math.min( y1 + 8, y2 ) )
         
         
                surface.DrawLine( x2, y1, math.max( x2 - 8, x1 ), y1 )
                surface.DrawLine( x2, y1, x2, math.min( y1 + 8, y2 ) )
         
         
                surface.DrawLine( x1, y2, math.min( x1 + 8, x2 ), y2 )
                surface.DrawLine( x1, y2, x1, math.max( y2 - 8, y1 ) )
         
         
                surface.DrawLine( x2, y2, math.max( x2 - 8, x1 ), y2 )
                surface.DrawLine( x2, y2, x2, math.max( y2 - 8, y1 ) )
            end    
    end
end)


