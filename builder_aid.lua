if SERVER then AddCSLuaFile() return end


--==--==-- SANDVICH'S BUILER INFO TOOL --==--==--
/*                                             
Made by me (The Sandvich Maker) to provide useful
on-screen information for myself and anybody else
who wants it.
*/
--==--==--==--==--==--==--==--==--==--==--==--==-

--==-- FONTS --==--

surface.CreateFont("builderfont", {
    font    = "Tahoma",
    size    = 12,
    weight  = 1000,
    antialias = true,
    shadow = false
} )

--==-- FUNCTIONS --==--

local function angToPos( rad, ang ) 
    local angle = math.rad( ang ) 
    return rad * math.cos( angle ), rad * math.sin( angle ) 
end

local function getAng( x1, y1, x2, y2 ) 
    return ( math.deg( math.atan2( y2 - y1, x2 - x1 ) ) + 90 ) % 360
end

local function boxSize( ent ) 
    return ent:OBBMaxs() - ent:OBBMins()
end

local function roundVector( vec ) 
    return Vector( math.Round( vec.x ), math.Round( vec.y ), math.Round( vec.z ) )
end

local function trimDecimals( message )
    local add  = 1
    if string.find( message, "-" ) then add = 2 end
    return string.Left( message, string.find( message, "." ) + add)
end

local function tostringVector( vec )
    return ( trimDecimals( tostring( vec.x ) ) .. ", " .. trimDecimals( tostring( vec.y ) ) .. ", " .. trimDecimals( tostring( vec.z ) ) )
end

local function tostringAngle( ang )
    return ( trimDecimals( tostring( ang.p ) ) .. ", " .. trimDecimals( tostring( ang.y ) ) .. ", " .. trimDecimals( tostring( ang.r ) ) )
end

local function tostringColor( col )
    return ( col.r .. ", ".. col.g .. ", " .. col.b .. ", " .. col.a )
end

local function drawList( x, y, list, color1, color2, coloroutline )
    local offset = 0
    for k, v in pairs( list ) do
        local name = v[1]
        local data = v[2]

        if tostring( data ) != "" and data != nil then
            offset = offset + 1
            draw.SimpleTextOutlined( name .. ":", "builderfont", x, y + offset * 10, color1, 0, 0, 1, coloroutline )
            draw.SimpleTextOutlined( tostring( data ), "builderfont", x + 70, y + offset * 10, color2, 0, 0, 1, coloroutline )
        end
    end

    surface.SetDrawColor( 0, 0, 0, 255 )
end

local function drawThickLineOutline( x1, y1, x2, y2, color )
    local ang = getAng( x1, y1, x2, y2 )
    local offset_x, offset_y = angToPos( 1, ang )

    surface.SetDrawColor( 0, 0, 0, color.a )
    surface.DrawLine( x1 + offset_x, y1 + offset_y, x2 + offset_x, y2 + offset_y )
    surface.DrawLine( x1 - offset_x, y1 - offset_y, x2 - offset_x, y2 - offset_y )
    surface.SetDrawColor( color )
    surface.DrawLine( x1, y1, x2, y2 )
end

local function drawThickLine( x1, y1, x2, y2, color )
    local ang = getAng( x1, y1, x2, y2 )
    local offset_x, offset_y = angToPos( 1, ang )

    surface.SetDrawColor( color )
    surface.DrawLine( x1, y1, x2, y2 )
    surface.DrawLine( x1 + offset_x, y1 + offset_y, x2 + offset_x, y2 + offset_y )
    surface.DrawLine( x1 - offset_x, y1 - offset_y, x2 - offset_x, y2 - offset_y )
end

local function drawArrow( x1, y1, x2, y2, color, outline )
    local arrow_1_x, arrow_1_y = angToPos( math.Distance( x1, y1, x2, y2 ) / 8, getAng( x1, y1, x2, y2 ) + 45 )
    local arrow_2_x, arrow_2_y = angToPos( math.Distance( x1, y1, x2, y2 ) / 8, getAng( x1, y1, x2, y2 ) + 135 )

    local drawFunc = ( outline and drawThickLineOutline or drawThickLine )
    drawFunc( x1, y1, x2, y2, color )
    drawFunc( x2, y2, x2 + arrow_1_x, y2 + arrow_1_y, color )
    drawFunc( x2, y2, x2 + arrow_2_x, y2 + arrow_2_y, color )
end

--==-- CONCOMMANDS --==--

local enabled = CreateClientConVar( "buildtool_enabled", 1, true, false )
local enable_guides = CreateClientConVar( "buildtool_guides", 1, true, false )
local range = CreateClientConVar( "buildtool_range", 500, true, false )
local enable_info = CreateClientConVar( "buildtool_info", 1, true, false )
local always_on = CreateClientConVar( "buildtool_alwayson", 0, true, false )
local arrow_outline = CreateClientConVar( "buildtool_arrowoutline", 1, true, false )

--==-- CUSTOMIZABLE VARIABLES --==--

local activeweapons = { }
activeweapons["weapon_physgun"] = 1
activeweapons["gmod_tool"] = 1

--==-- UNCUSTOMIZABLE VARIABLES --==--

local ply = LocalPlayer()

--==-- DRAWING --==--

hook.Add("HUDPaint", "builder_tool", function() 
    if enabled:GetBool() then 
        local trace = util.TraceLine( util.GetPlayerTrace( ply ) )

        if trace.HitNonWorld then
            local target = trace.Entity
            if !target:IsPlayer() and ( activeweapons[ply:GetActiveWeapon():GetClass()] or always_on:GetInt() == 1 ) and target:IsValid() then 
                local boxsize = boxSize( target )
                local origin = target:GetPos()
                local origin_screen = origin:ToScreen()
                local dist = ply:GetShootPos():Distance( target:GetPos() )
                local frac = 1 - math.Clamp( dist / range:GetInt(), 0, 1 )

                -- if in range
                if ply:GetPos():Distance( target:GetPos() ) < range:GetInt() then

                    -- drawing the guides 
                    if enable_guides:GetBool() then
                        surface.DrawCircle( origin_screen.x, origin_screen.y, 5, Color( 255, 100, 100, 255 * frac ) )

                        local guide = target:LocalToWorld( Vector( boxsize.x / 2 + 5, 0, 0 ) ):ToScreen()
                        drawArrow( origin_screen.x, origin_screen.y, guide.x, guide.y, Color( 255, 100, 100, 255 * frac ), arrow_outline:GetBool() )

                        guide = target:LocalToWorld( Vector( 0, boxsize.y / 2 + 5, 0 ) ):ToScreen()
                        drawArrow( origin_screen.x, origin_screen.y, guide.x, guide.y, Color( 100, 255, 100, 255 * frac ), arrow_outline:GetBool() )

                        guide = target:LocalToWorld( Vector( 0, 0, boxsize.z / 2 + 5 ) ):ToScreen()
                        drawArrow( origin_screen.x, origin_screen.y, guide.x, guide.y, Color( 100, 100, 255, 255 * frac ), arrow_outline:GetBool() )
                    end

                    -- drawing text info
                    if enable_info:GetBool() then   
                        local info_list = {
                            { "CLASS", target:GetClass() },
                            { "MODEL", target:GetModel() },
                            { "MATERIAL", target:GetMaterial() },
                            { "COLOR", tostringColor( target:GetColor() ) },
                            { "BOX SIZE", tostringVector( boxsize ) },
                            { "POSITION", tostringVector( target:GetPos() ) },
                            { "ANGLE", tostringAngle( target:GetAngles() ) }
                        }
                        drawList( origin_screen.x + 70 - dist / 16, origin_screen.y - 130 + dist / 16, info_list, Color( 255, 0, 0, 255 * frac ), Color( 255, 255, 255, 255 * frac ), Color( 0, 0, 0, 255 * frac ) )
                    end
                end 
            end
        end
    end
end )