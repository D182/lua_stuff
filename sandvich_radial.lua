if SERVER then AddCSLuaFile() return end

   /////-----------\\\\\
  ///// RADIAL MENU \\\\\
 /////---------------\\\\\

-- by The Sandvich Maker --
-- Weird invisibility bug fixed by Specci! --

//for i = 16, 22 do
    surface.CreateFont("radialfont", {
        font    = "Tahoma",
        size    = 18,
        weight  = 1000,
        antialias = true,
        shadow = false
    })
//end

----- functions ------

local function getAng( x1, y1, x2, y2 ) 
    return ( math.deg( math.atan2( y2 - y1, x2 - x1 ) ) + 90 ) % 360
end

local function angToPos( rad, ang ) 
    local angle = math.rad( ang ) 
    return rad * math.cos( angle ), rad * math.sin( angle ) 
end

local function drawCircle( x, y, radius, color, quality )
    local poly = { }
    local num = 0

    for i = 1, quality do
        num = math.rad( i * 360 ) / quality
        poly[i] = { x = x + math.cos( num ) * radius, y = y + math.sin( num ) * radius }
    end
    surface.SetDrawColor( color )
    draw.NoTexture()
    surface.DrawPoly( poly )
end

-------- settings ----------

local radial_elements = {
    {"weapon", "Physics Gun", "weapon_physgun"},
    {"tool", "Expression 2", "wire_expression2"},
    {"tool", "Advanced Wire", "wire_adv"},
    {"tool", "Debugger", "wire_debugger"},
    {"tool", "Precision", "precision"},
    {"concommand", "Clear Decals", "ev", "decals"},
    {"concommand", "Open Menu", "+radial_options"}
}

local count = table.Count( radial_elements )
local offset_target = { }
local offset = { }

for i = 1, count do 
    offset_target[i] = 0
    offset[i] = 0
end

local active = 0
local radius = 200
local use_sound = true
local circle_quality = 30
local use_qualityeffect = false

-- colors

local colors = { }
colors["tool"] = { 
    Color( 255, 255, 255, 255 ), --text color
    Color( 50, 200, 100, 255 ), --highlighted text color
    Color( 25, 25, 25, 255 ), --inner circle color
    Color( 45, 47, 45, 255 ), --outer circle and rest color
    Color( 50, 200, 100, 235 ), --circle glow color
    Color( 50, 150, 75, 255), --cursor color
    Color( 45, 47, 45, 100 ) --inner circle glow
}
colors["concommand"] = { 
    Color( 255, 255, 255, 255 ), --text color
    Color( 50, 100, 200, 255 ), --highlighted text color
    Color( 25, 25, 25, 255 ), --inner circle color
    Color( 45, 45, 47, 255 ), --outer circle and rest color
    Color( 50, 100, 200, 235 ), --circle glow color
    Color( 50, 75, 150, 255), --cursor color
    Color( 45, 45, 47, 100 ) --inner circle glow
}
colors["weapon"] = { 
    Color( 255, 255, 255, 255 ), --text color
    Color( 200, 75, 50, 255 ), --highlighted text color
    Color( 25, 25, 25, 255 ), --inner circle color
    Color( 47, 45, 45, 255 ), --outer circle and rest color
    Color( 200, 50, 50, 235 ), --circle glow color
    Color( 150, 50, 50, 255), --cursor color
    Color( 47, 45, 45, 100 ) --inner circle glow
}

----- functionality -----

local oldselect = 0

concommand.Add( "+radial_menu", function() 
    active = 1 

    gui.EnableScreenClicker( true )

    hook.Add( "HUDShouldDraw", "radial_removecrosshair", function( name )
        if name == "CHudCrosshair" then return false end
    end)
end )
concommand.Add( "-radial_menu", function()
    active = 0
    
    local m_x, m_y = gui.MousePos()
    local select_ang = getAng( ScrW() / 2, ScrH() / 2, m_x, m_y )
    local selected = math.Clamp( math.ceil( ( ( ( select_ang + ( 180 / count ) ) % 360 ) / 360 ) * count ), 1, count )

    local element_type = radial_elements[selected][1]

    if element_type == "tool" then 
        RunConsoleCommand( "use", "gmod_tool" )
        RunConsoleCommand( "gmod_toolmode", radial_elements[selected][3] )
    elseif element_type == "concommand" then
        RunConsoleCommand( radial_elements[selected][3], radial_elements[selected][4] )
    elseif element_type == "weapon" then 
        RunConsoleCommand( "use", radial_elements[selected][3] )
    end

    if use_sound then surface.PlaySound( "buttons/button24.wav" ) end

    hook.Remove( "HUDShouldDraw", "radial_removecrosshair" )

    gui.EnableScreenClicker( false )
end )

---- visuals ----

hook.Add("HUDPaint", "sandvich_radial", function()
    if active == 1 then
        local m_x, m_y = gui.MousePos()
        local select_ang = getAng( ScrW() / 2, ScrH() / 2, m_x, m_y )
        
        local selected = math.Clamp( math.ceil( ( ( ( select_ang + ( 180 / count ) ) % 360 ) / 360 ) * count ), 1, count )

        if oldselect != selected and use_sound == true then surface.PlaySound( "buttons/lightswitch2.wav" ) end

        oldselect = selected

        for i = 1, count do
            local color = colors[radial_elements[i][1]]

            local ang = -90 + ( (360 / count ) * ( i - 1 ) )
            
            local col = color[1]
            offset_target[i] = 0
            if selected == i then 
                col = color[2]
                offset_target[i] = 20
            end

            offset[i] = offset[i] + ( offset_target[i] - offset[i] ) / 2

            local x, y = angToPos( radius + offset[i], ang )
            local qualityoffset = 0
            if use_qualityeffect then qualityoffset = offset_target[i] * 2 end

            drawCircle( x + ScrW() / 2, y + ScrH() / 2, 56, color[5], circle_quality + qualityoffset  )

            local offsetx, offsety = angToPos( 20 + offset[i], ang )
            local poly_x1, poly_y1 = angToPos( radius - 70, ang - ( 15 / count ) ) 
            local poly_x2, poly_y2 = angToPos( radius - 70, ang + ( 15 / count ) ) 
            surface.SetDrawColor( color[4] )
            surface.DrawPoly( { { x = ScrW() / 2 + offsetx, y = ScrH() / 2 + offsety }, { x = ScrW() / 2 + poly_x1 + offsetx, y = ScrH() / 2 + poly_y1 + offsety }, { x = ScrW() / 2 + poly_x2 + offsetx, y = ScrH() / 2 + poly_y2 + offsety } } )

            drawCircle( x + ScrW() / 2, y + ScrH() / 2, 52, color[4], circle_quality + qualityoffset )
            drawCircle( x + ScrW() / 2, y + ScrH() / 2, 48, color[3], circle_quality + qualityoffset )
            draw.SimpleTextOutlined( radial_elements[i][2], "radialfont", x + ScrW() / 2, y + ScrH() / 2, col, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 2, Color(0,0,0,255) )
        end

        local color = colors[radial_elements[selected][1]]

        drawCircle( ScrW() / 2, ScrH() / 2, 27, color[7], circle_quality )
        drawCircle( ScrW() / 2, ScrH() / 2, 22, color[4], circle_quality )
        local offsetx, offsety = angToPos( 20, ( select_ang - 90 ) % 360 )
        local poly_x1, poly_y1 = angToPos( 40, ( ( select_ang - 90 ) % 360 ) - ( 50 / count ) ) 
        local poly_x2, poly_y2 = angToPos( 40, ( ( select_ang - 90 ) % 360 ) + ( 50 / count ) ) 
        surface.SetDrawColor( color[6] )
        surface.DrawPoly( { { x = ScrW() / 2 + poly_x1, y = ScrH() / 2 + poly_y1 }, { x = ScrW() / 2 + poly_x2, y = ScrH() / 2 + poly_y2 }, { x = ScrW() / 2 + offsetx * 4, y = ScrH() / 2 + offsety * 4} } )
    end
end)


  /////------------\\\\\
 ///// OPTIONS MENU \\\\\
/////----------------\\\\\


concommand.Add( "+radial_options", function()
    local optionspanel = vgui.Create( "DFrame" )
    optionspanel:SetPos( ScrW() / 2 - 250, ScrH() / 2 - 150 )
    optionspanel:SetSize( 500, 300 )
    optionspanel:SetTitle( "Sandvich's Radial Menu - Options" )
    optionspanel:SetVisible( true )
    optionspanel:SetDraggable( true )
    optionspanel:ShowCloseButton( true )
    optionspanel:MakePopup()

    local tabs = vgui.Create( "DPropertySheet" )
    tabs:SetParent( optionspanel )
    tabs:SetPos( 5, 30 )
    tabs:SetSize( 490, 265 )

    local SheetItemOne = vgui.Create( "DCheckBoxLabel" )
    SheetItemOne:SetText( "Duck off derma" )
    SheetItemOne:SetConVar( "some_convar" )
    SheetItemOne:SetValue( 1 )
    SheetItemOne:SizeToContents()

    tabs:AddSheet( "Radial Elements", SheetItemOne, "gui/silkicons/user", false, false, "Add / Remove / Change options on the menu." )
     
end )

concommand.Add( "-radial_options", function()
    print( "exit" )
end )