--[[
    Radial Tool Menu by D182
    Special Thanks to The Sandvich Maker
    
    Edit Settings on Line 56 - 69

    Fading and derma gui by Arena
]]--

local settings = {}

function saveConfig()
    file.Write("radial_settings.txt",util.TableToKeyValues(settings))
end

-- Preferences --

settings.clrs = {}

function coloursDefault()
    settings.clrs.back = Color(50, 50, 50, 240) -- Back of label
    settings.clrs.fore = Color(255, 255, 255) -- Font on label
    settings.clrs.fade = Color(85, 25, 25, 255) -- Gradient color (when highlighted)
    settings.clrs.line = Color(125, 35, 35, 255) -- Line color
end

-- ----------- --

local sm = {}

surface.CreateFont( "hud_font", {
    font = "Trebuchet",
    size = 15,
    weight = 500,
    outline = false
} )
 
local function posToAng( x1, y1, x2, y2 )
    return ( math.deg( math.atan2( y2 - y1, x2 - x1 ) ) - 90 ) % 360
end
 
local function angToPos( rad, ang )
    local angle = math.rad( ang )
    return rad * math.cos( angle ), rad * math.sin( angle )
end

local function wordBox(x, y, text, bgcolor, index)
    
    surface.SetFont("hud_font")
    
    local len,_ = surface.GetTextSize(text)

    surface.SetDrawColor(settings.clrs.back)
    surface.DrawRect(x - (len/2), y - 5, len + 12, 20)
    
    surface.SetMaterial(Material("gui/gradient_up"))
    
    if sm[index] then
        surface.SetDrawColor( Color( settings.clrs.fade.r, settings.clrs.fade.g, settings.clrs.fade.b, sm[index] ) )
        sm[index] = sm[index] + (255 - sm[index]) / 16
    else
        surface.SetDrawColor(Color(0,0,0,0))
    end
    
    surface.DrawTexturedRect( x - (len/2), y - 5, len + 12, 20 )

    draw.SimpleText(text, "hud_font", x - (len/2) + 5, y - 2, settings.clrs.fore, TEXT_ALIGN_LEFT)
end
 
function math.inrange(val, min, max)
    return val <= max and val >= min
end
 
local function switchtool(tool)
    RunConsoleCommand("use", "gmod_tool")
    RunConsoleCommand("gmod_toolmode", tool)
end

local function switchwep(wep)
    RunConsoleCommand("use", wep)
end     

local function runcommand(cmd)
    local command = string.Explode(" ", cmd)
    RunConsoleCommand(command[1], command[2], command[3], command[4])
end    
------------------------------
 
-----------Settings-----------
 
local radius = 150
local deadzone = 90
settings.items = {}

function itemsDefault()
    settings.items = {
        {"tool","wire_expression2", "Expression 2"},
        {"tool","wire_adv", "Wire Adv"},
        {"tool","wire_debugger", "Debugger"},
        {"tool","adv_duplicator", "Adv Duplicator"},
        {"tool","weld", "Weld"},
        {"tool","nocollide", "No Collide"},
        {"concommand","r_cleardecals", "Clear Decals"},
        {"concommand","stopsound", "Stop Sounds"},
        {"weapon", "weapon_physgun", "Physgun"}           
    }
end
if file.Exists("radial_settings.txt", "DATA") then
    local readFile = file.Read("radial_settings.txt", "DATA")
    settings = util.KeyValuesToTable(readFile);
else
    itemsDefault()
    coloursDefault()
    saveConfig()
end
------------------------------
 
local newtool = nil
local cmd = nil
local active = false
 
hook.Add("HUDPaint", "RadialMenu", function()
    if active then
        local count = table.Count(settings.items)
        local ctrx, ctry = ScrW() / 2, ScrH() / 2
        local msx, msy = gui.MousePos()
        local mouse_ang = posToAng(msx, msy, ctrx, ctry)
        local index

        if math.inrange(msx-ctrx, -deadzone, deadzone) and math.inrange(msy-ctry, -deadzone, deadzone) then
            index = 0
            newtool = nil
        else
            index = math.Clamp( math.ceil( ( ( ( mouse_ang + ( 180 / count ) ) % 360 ) / 360 ) * count ), 1, count )   
            newtool = {settings.items[index][1], settings.items[index][2]} 
        end

        for k,v in pairs(settings.items) do --draw tools
            local targalpha = 0
            if k == index then if !sm[k] then sm[k] = 1 end targalpha = 255 else sm[k] = 1 targalpha = 0 end

            local toolname = v[3]
            local ang = -90 + ( (360 / count ) * ( k - 1 ) )
            local x, y = angToPos(radius, ang)
 
            surface.SetDrawColor( settings.clrs.line )
           
            if settings.snap then
                if k == index then 
                    surface.DrawLine(ctrx, ctry, x + ctrx, y + ctry - 20) 
                end
            else
                surface.DrawLine(ctrx, ctry, msx, msy)
            end
            wordBox( x + ctrx, y + ctry - 20, toolname, clr, k)
        end
    end
end)    
 
concommand.Add("+radial_menu", function()
    active = true
    gui.EnableScreenClicker(true)
end)
concommand.Add("-radial_menu", function()
    active = false
    gui.EnableScreenClicker(false)
    
    if newtool != nil then
        if newtool[1] == "tool" then
            switchtool(newtool[2]) 
        elseif newtool[1] == "weapon" then 
            switchwep(newtool[2])
        elseif newtool[1] == "concommand" then 
            runcommand(newtool[2])
        end           
    end
end)

function DermaMake()

    if rmf then rmf:Remove() end -- If frame already exists remove it, nubway but idc

    rmf = vgui.Create("DFrame") -- Frame
    rmf:SetTitle("Radial Menu Customization")
    rmf:SetSize(350, 300)
    local Size_X, Size_Y = rmf:GetSize()
    rmf:SetPos(ScrW()/2 - Size_X/2, ScrH()/2 - Size_Y/2)
    rmf:SetDeleteOnClose(true)
    rmf:SetBackgroundBlur(true)
    rmf:MakePopup()

    local ps = vgui.Create("DPropertySheet", rmf) -- Property Sheet (Tabs)
    ps:SetPos(5, 30)
    ps:SetSize(330, 265)

    local p_clrs = vgui.Create("DPanel") -- Panel to hold everything inside of the colours tab
    p_clrs:SizeToContents()
    
    local p_tools = vgui.Create("DPanel") -- Panel to hold everything inside of the tools tab
    p_tools:SizeToContents()
    
    local p_other = vgui.Create("DPanel") -- Panel to hold everything inside of the other tab
    p_other:SizeToContents()
    
    local cb_snp = vgui.Create("DCheckBoxLabel", p_other)
    cb_snp:SetPos(10, 50)
    cb_snp:SetSize(128,25)
    cb_snp:SetChecked(settings.snap)
    cb_snp:SetText("Snapping?")
    cb_snp:SetTextColor(Color(0, 0, 0))
    
    cb_snp.OnChange = function()
        settings.snap = cb_snp:GetChecked()
        saveConfig()
    end
    
    local tls_lv = vgui.Create("DListView", p_tools)
    tls_lv:SetPos(5, 5)
    tls_lv:SetSize(305, 215)
    tls_lv:SetMultiSelect(false)
    tls_lv:AddColumn("Type")
    tls_lv:AddColumn("Args")
    tls_lv:AddColumn("Label")
    
    local nt_btn = vgui.Create("DButton", p_tools)
    nt_btn:SetText("..")
    nt_btn:SetSize(16, 16)
    nt_btn:SetPos(294, 204)
    
    nt_btn.DoClick = function()
        local cm = DermaMenu()
        
        cm:AddOption("New Entry", function()
            local f_ne = vgui.Create("DFrame")
            f_ne:SetTitle("New Entry")
            f_ne:SetSize(362, 55)
            f_ne:ShowCloseButton(false)
            
            local f_sx, f_sy = f_ne:GetSize()
            f_ne:SetDeleteOnClose(true)
            
            local f_cb = vgui.Create("DComboBox", f_ne)
            f_cb:SetPos(5, 28)
            f_cb:SetSize(100, 20)
            f_cb:AddChoice("tool")
            f_cb:AddChoice("concommand")
            f_cb:AddChoice("weapon")
            f_cb:SetToolTip("Type")
            
            local f_if = vgui.Create("DTextEntry", f_ne)
            f_if:SetPos(106, 28)
            f_if:SetSize(100, 20)
            f_if:SetToolTip("Arguments")
            
            local f_in = vgui.Create("DTextEntry", f_ne)
            f_in:SetPos(210, 28)
            f_in:SetSize(100, 20)
            f_in:SetToolTip("Label")
            
            local f_ok = vgui.Create("DButton", f_ne)
            f_ok:SetSize(18, 18)
            f_ok:SetText("+")
            f_ok:SetToolTip("Accept")
            f_ok:SetPos(314, 28)
            
            local f_no = vgui.Create("DButton", f_ne)
            f_no:SetSize(18, 18)
            f_no:SetText("X")
            f_no:SetToolTip("Cancel")
            f_no:SetPos(334, 28)
            
            f_no.DoClick = function()
                f_ne:Remove()
            end
            
            f_ok.DoClick = function()
                local pootis = {f_cb:GetValue(),f_if:GetValue(),f_in:GetValue()}
                table.insert(settings.items, pootis)
                saveConfig()
                
                f_ne:Remove()
                refreshTools()
            end
            
            f_ne:SetPos(ScrW()/2 - f_sx/2, ScrH()/2 - f_sy/2)
            f_ne:MakePopup()
        end)
        
        cm:AddOption("Defaults", function()
            itemsDefault()
            refreshTools()
            saveConfig()
        end)
        
        cm:Open()
    end
    
    tls_lv.OnRowRightClick = function(panel, line) -- Rightclick on line function
        local cm = DermaMenu()
        
        cm:AddOption("Edit", function()
            local f_ed = vgui.Create("DFrame")
            f_ed:SetTitle(tls_lv:GetLine(line):GetValue(3))
            f_ed:SetSize(360, 55)
            f_ed:ShowCloseButton(false)
            
            local f_sx, f_sy = f_ed:GetSize()
            f_ed:SetDeleteOnClose(true)
            
            local f_cb = vgui.Create("DComboBox", f_ed)
            f_cb:SetPos(5, 28)
            f_cb:SetSize(100, 20)
            f_cb:AddChoice("tool")
            f_cb:AddChoice("concommand")
            f_cb:AddChoice("weapon")
            f_cb:SetText(tls_lv:GetLine(line):GetValue(1))
            f_cb:SetToolTip("Type")
            
            local f_if = vgui.Create("DTextEntry", f_ed)
            f_if:SetPos(106, 28)
            f_if:SetSize(100, 20)
            f_if:SetText(tls_lv:GetLine(line):GetValue(2))
            f_if:SetToolTip("Arguments")
            
            local f_in = vgui.Create("DTextEntry", f_ed)
            f_in:SetPos(210, 28)
            f_in:SetSize(100, 20)
            f_in:SetText(tls_lv:GetLine(line):GetValue(3))
            f_in:SetToolTip("Label")
            
            local f_ok = vgui.Create("DImageButton", f_ed)
            f_ok:SetSize(18, 18)
            f_ok:SetImage("icon16/accept.png")
            f_ok:SetToolTip("Accept")
            f_ok:SetPos(314, 28)
            
            local f_no = vgui.Create("DImageButton", f_ed)
            f_no:SetSize(18, 18)
            f_no:SetImage("icon16/cancel.png")
            f_no:SetToolTip("Cancel")
            f_no:SetPos(334, 28)
            
            f_no.DoClick = function()
                if IsValid(f_ed) then
                    f_ed:Remove()
                end
            end
            
            f_ok.DoClick = function()
                local pootis = {f_cb:GetValue(),f_if:GetValue(),f_in:GetValue()}
                settings.items[line] = pootis
                saveConfig()
                
                f_ed:Remove()
                refreshTools()
            end
            
            f_ed:SetPos(ScrW()/2 - f_sx/2, ScrH()/2 - f_sy/2)
            f_ed:MakePopup()
        end)
        
        cm:AddOption("Remove", function()
        table.remove(settings.items, line)
        refreshTools()
        saveConfig()
        end)
    
    cm:Open()
    end

    function refreshTools()
        tls_lv:Clear()
        
        for i=1,table.Count(settings.items) do
            tls_lv:AddLine(settings.items[i][1], settings.items[i][2], settings.items[i][3])
        end
        
        tls_lv:SortByColumn(1)
    end
    refreshTools()
    
    local rad_ns = vgui.Create("DSlider", p_other) -- Radius slider
    rad_ns:SetPos(10, 30)
    --rad_ns:SetWide(150)
    --rad_ns:SetText("Radius")
    --rad_ns:SetMin(125)
    --rad_ns:SetMax(ScrW()/2)
    --rad_ns:SetDecimals(0)

    local cus_lv = vgui.Create("DListView", p_clrs)
    cus_lv:SetPos(5, 5)
    cus_lv:SetSize(305, 215)
    cus_lv:SetMultiSelect(false)
    cus_lv:AddColumn("Name")
    cus_lv:AddColumn("Variable")
    cus_lv:AddColumn("Colour")
    
    function refreshColours()
        cus_lv:Clear()
    
        cus_lv:AddLine("Background", "back", settings.clrs.back.r .. "," .. settings.clrs.back.g .. "," .. settings.clrs.back.b .. "," .. settings.clrs.back.a)
        cus_lv:AddLine("Text", "fore", settings.clrs.fore.r .. "," .. settings.clrs.fore.g .. "," .. settings.clrs.fore.b .. "," .. settings.clrs.fore.a)
        cus_lv:AddLine("Gradient", "fade", settings.clrs.fade.r .. "," .. settings.clrs.fade.g .. "," .. settings.clrs.fade.b .. "," .. settings.clrs.fade.a)
        cus_lv:AddLine("Line", "line", settings.clrs.line.r .. "," .. settings.clrs.line.g .. "," .. settings.clrs.line.b .. "," .. settings.clrs.line.a)
   end

    refreshColours()
        
    cus_lv.OnRowRightClick = function(panel, line) -- Rightclick on line function
        local cm = DermaMenu()
        cm:AddOption("Customize", function()
        
        local f_cmx = vgui.Create("DFrame")
        f_cmx:SetTitle(cus_lv:GetLine(line):GetValue(1))
        f_cmx:SetSize(270, 285)
        local f_sx, f_sy = f_cmx:GetSize()
        f_cmx:SetDeleteOnClose(true)
        
        local cmx = vgui.Create( "DColorMixer", f_cmx)
        cmx:SetPos(5, 30)

        local btn_a = vgui.Create("DImageButton", f_cmx)
        btn_a:SetToolTip("Accept")
        btn_a:SetPos(4, 263)
        btn_a:SetSize(18, 18)
        btn_a:SetImage("icon16/accept.png")



        btn_a.DoClick = function()
        
            settings.clrs[cus_lv:GetLine(line):GetValue(2)] = cmx:GetColor()

            saveConfig()

            refreshColours()
            
            if IsValid(f_cmx) then f_cmx:Remove() end
        end

        local btn_c = vgui.Create("DImageButton", f_cmx)
        btn_c:SetToolTip("Cancel")
        btn_c:SetPos(24, 263)
        btn_c:SetSize(18, 18)
        btn_c:SetImage("icon16/cancel.png")
        btn_c.DoClick = function() 
            if IsValid(f_cmx) then 
                f_cmx:Remove() 
            end 
        end
        
        f_cmx:SetPos(ScrW()/2 - f_sx/2, ScrH()/2 - f_sy/2)
        f_cmx:ShowCloseButton(false)
        f_cmx:MakePopup()
    end)
    cm:Open()

end

ps:AddSheet("Colours", p_clrs, "icon16/user.png", false, false, "Colour cusotmization.") -- Add colours panel to tabs
ps:AddSheet("Tools", p_tools, "icon16/wrench.png", false, false, "Tools, weapons & concommands.")
ps:AddSheet("Other", p_other, "icon16/magnifier.png", false, false, "Other settings, WIP.") -- Add other panel to tabs

end
concommand.Add("radial_dmenu",function() DermaMake() end)
